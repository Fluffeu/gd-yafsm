@tool
extends Condition
class_name TimeoutCondition

signal value_changed(new_value)

@export var value: float:
	set = set_value,
	get = get_value

var reset_t: int = 0


func set_value(v):
	if value != v:
		value = v
		emit_signal("value_changed", v)
		emit_signal("display_string_changed", "dupa3000")


func _init(p_name=""):
	super._init(p_name)


func reset() -> void:
	reset_t = Time.get_ticks_msec()


func is_fulfilled() -> bool:
	return Time.get_ticks_msec() - reset_t > value*1000.0


func get_value():
	return value


func get_value_string():
	return get_value()


func display_string():
	return ""
