extends ColorRect

var flowchart
	
func _ready():
	flowchart = get_parent().get_parent()
	material = ShaderMaterial.new()
	material.shader = load("res://addons/imjp94.yafsm/scenes/flowchart/FlowChartGrid.gdshader")
	queue_redraw.call_deferred()

func _draw():
	self.position = flowchart.position
	self.size = flowchart.size*100  # good with min_zoom = 0.5 e max_zoom = 2.0
	material.set_shader_parameter("color_major", flowchart.grid_major_color)
	material.set_shader_parameter("color_minor", flowchart.grid_minor_color)
	material.set_shader_parameter("offset", -flowchart.size - position)
	material.set_shader_parameter("scale", 1.0/flowchart.zoom)
