@tool
extends "ConditionEditor.gd"
const Utils = preload("../../scripts/Utils.gd")
const Comparation = preload("../../src/conditions/ValueCondition.gd").Comparation

@onready var comparation_button = $Comparation
@onready var comparation_popup_menu = $Comparation/PopupMenu
@onready var value = $TimeValue

var _old_value = 0.0

func _ready():
	super._ready()
	
	value.value_changed.connect(_on_value_changed)
	value.focus_entered.connect(_on_value_focus_entered)
	value.focus_exited.connect(_on_value_focus_exited)
	set_process_input(false)

func _input(event):
	super._input(event)
	
	if event is InputEventMouseButton:
		if event.pressed:
			if get_viewport().gui_get_focus_owner() == value:
				var local_event = value.make_input_local(event)
				if not value.get_rect().has_point(local_event.position):
					value.release_focus()

func _on_value_changed(new_value):
	change_value_action(_old_value, new_value)
	value.release_focus()

func _on_value_focus_entered():
	set_process_input(true)
	_old_value = value.value

func _on_value_focus_exited():
	set_process_input(false)
	change_value_action(_old_value, value.value)

func _on_condition_changed(new_condition):
	super._on_condition_changed(new_condition)
	if new_condition:
		value.value = new_condition.value

func set_value(v):
	if condition.get_value() != v:
		condition.set_value(v)
		_on_value_changed(v)

func change_value_action(from, to):
	if from == to:
		return
	undo_redo.create_action("Change Condition Value")
	undo_redo.add_do_method(self, "set_value", to)
	undo_redo.add_undo_method(self, "set_value", from)
	undo_redo.commit_action()
