This a fork of plugin for Godot 4 - YASFM ([original project on github](https://github.com/imjp94/gd-YAFSM))

Additions to the original version:
- editor panel's background rendered using shaders for better performance
- time conditions for state transitions
